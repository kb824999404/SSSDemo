#include <common>
attribute vec2 uv2;
attribute vec4 tangent;

varying vec2 vUv;
varying vec2 vUv2;
varying vec3 vNormal;
varying vec3 vViewPosition;
varying vec3 vTangent;
varying vec3 vBitangent;

void main(){

    // uv_vertex
	vUv = uv;
    // uv2_vertex
    vUv2 = uv2;

    // project_vertex
    vec3 transformed = vec3( position );
    vec4 mvPosition = vec4( transformed, 1.0 );
    mvPosition = modelViewMatrix * mvPosition;

    vViewPosition = - mvPosition.xyz;

    gl_Position = projectionMatrix * mvPosition;



    // beginnormal_vertex
    vec3 objectNormal = vec3( normal );
    vec3 objectTangent = vec3( tangent.xyz );

    // defaultnormal_vertex
    vec3 transformedNormal = objectNormal;
    transformedNormal = normalMatrix * transformedNormal;
    vec3 transformedTangent = ( modelViewMatrix * vec4( objectTangent, 0.0 ) ).xyz;

    // normal_vertex
    vNormal = normalize( transformedNormal );
    vTangent = normalize( transformedTangent );
    vBitangent = normalize( cross( vNormal, vTangent ) * tangent.w );


}