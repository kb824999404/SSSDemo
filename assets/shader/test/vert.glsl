varying vec2 vTexCoord;
varying vec3 vViewPosition;
varying vec3 vWorldPosition;

void main(){
    vTexCoord = uv;

    vec4 worldPosition = modelMatrix * vec4( position, 1.0 );
    vWorldPosition = worldPosition.xyz;

    vec4 viewPosition = viewMatrix * worldPosition;
    vViewPosition = viewPosition.xyz;


    gl_Position = projectionMatrix * viewPosition;
}