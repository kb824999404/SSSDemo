varying vec2 vTexCoord;
varying vec3 vViewPosition;
varying vec3 vWorldPosition;

void main(void){
    gl_FragColor = vec4(vTexCoord,1.0,1.0);
}